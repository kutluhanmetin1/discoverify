package com.kmetin.discoverify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiscoverifyApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscoverifyApplication.class, args);
	}
}
