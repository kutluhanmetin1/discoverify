package com.kmetin.discoverify.controller;

import com.kmetin.discoverify.service.SpotifyService;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Artist;
import com.wrapper.spotify.model_objects.specification.ArtistSimplified;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Controller
public class SpotifyController {
    private final SpotifyService spotifyService;

    public SpotifyController(SpotifyService spotifyService) {
        this.spotifyService = spotifyService;
    }

    @RequestMapping("/login")
    public ModelAndView login() {
        return new ModelAndView("redirect:" + spotifyService.getCallbackURL());
    }

    @RequestMapping("/callback")
    public String callback(@RequestParam("code") String authorizationCode, Model model) throws IOException, SpotifyWebApiException, ExecutionException, InterruptedException {
        spotifyService.authorizeUser(authorizationCode);
        Map<String, ArtistSimplified> usersSavedArtists = spotifyService.getUsersSavedArtists(1);
        HashMap<ArtistSimplified, List<Artist>> newArtists = spotifyService.findNewArtists(usersSavedArtists);

        model.addAttribute("newArtists", newArtists);
        return "Home";
    }
}
