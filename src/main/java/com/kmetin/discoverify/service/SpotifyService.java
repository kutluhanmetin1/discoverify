package com.kmetin.discoverify.service;

import com.neovisionaries.i18n.CountryCode;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.SpotifyHttpManager;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials;
import com.wrapper.spotify.model_objects.specification.Artist;
import com.wrapper.spotify.model_objects.specification.ArtistSimplified;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.SavedTrack;
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeRequest;
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeUriRequest;
import com.wrapper.spotify.requests.data.artists.GetArtistsRelatedArtistsRequest;
import com.wrapper.spotify.requests.data.library.GetUsersSavedTracksRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
public class SpotifyService {
    private SpotifyApi spotifyApi;

    private String clientId;
    private String clientSecret;
    private String callbackUrl;
    private String state;
    private String scope;

    public SpotifyService(@Value("${spotify.api.clientId}") String clientId,
                          @Value("${spotify.api.clientSecret}") String clientSecret,
                          @Value("${spotify.api.callbackUrl}") String callbackUrl,
                          @Value("${spotify.api.state}") String state,
                          @Value("${spotify.api.scope}") String scope){
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.callbackUrl = callbackUrl;
        this.state = state;
        this.scope = scope;

        spotifyApi = new SpotifyApi.Builder()
                .setClientId(this.clientId)
                .setClientSecret(this.clientSecret)
                .setRedirectUri(SpotifyHttpManager.makeUri(this.callbackUrl))
                .build();
    }

    public String getCallbackURL(){
        AuthorizationCodeUriRequest authorizationCodeUriRequest = spotifyApi.authorizationCodeUri()
                .state(state)
                .scope(scope)
                .show_dialog(true)
                .build();

        URI callbackURI = authorizationCodeUriRequest.execute();

        return callbackURI.toString();
    }

    public void authorizeUser(String authorizationCode) throws IOException, SpotifyWebApiException {
        AuthorizationCodeRequest authorizationCodeRequest = spotifyApi.authorizationCode(authorizationCode).build();

        AuthorizationCodeCredentials authorizationCodeCredentials = authorizationCodeRequest.execute();

        spotifyApi.setAccessToken(authorizationCodeCredentials.getAccessToken());
        spotifyApi.setRefreshToken(authorizationCodeCredentials.getRefreshToken());
    }

    public Map<String, ArtistSimplified> getUsersSavedArtists(int page) throws IOException, SpotifyWebApiException, ExecutionException, InterruptedException {
        List<SavedTrack> libraryTracks = getUsersSavedTracks(page);

        Map<String, ArtistSimplified> savedArtists = new HashMap<>();
        for(int i = 0; i < libraryTracks.size(); i++){
            savedArtists.put(libraryTracks.get(i).getTrack().getArtists()[0].getName(), libraryTracks.get(i).getTrack().getArtists()[0]);
        }

        return savedArtists;
    }

    private List<SavedTrack> getUsersSavedTracks(int offset) throws ExecutionException, InterruptedException {
        List<SavedTrack> libraryTracks = new ArrayList<>();

        GetUsersSavedTracksRequest getUsersSavedTracksRequest = spotifyApi.getUsersSavedTracks()
                .limit(50)
                .offset(offset)
                .market(CountryCode.SE)
                .build();

        Future<Paging<SavedTrack>> pagingFuture = getUsersSavedTracksRequest.executeAsync();
        Paging<SavedTrack> savedTrackPaging = pagingFuture.get();
        libraryTracks.addAll(Arrays.asList(savedTrackPaging.getItems()));

        return libraryTracks;
    }

    public HashMap<ArtistSimplified, List<Artist>> findNewArtists(Map<String, ArtistSimplified> usersSavedArtists) throws IOException, SpotifyWebApiException {
        HashMap<ArtistSimplified, List<Artist>> newArtistsMap = new HashMap<>();

        for(Map.Entry savedArtist: usersSavedArtists.entrySet()){
            ArtistSimplified artistSimplified = (ArtistSimplified) savedArtist.getValue();

            GetArtistsRelatedArtistsRequest getArtistsRelatedArtistsRequest = spotifyApi
                    .getArtistsRelatedArtists(artistSimplified.getId())
                    .build();

            Artist[] relatedArtists = getArtistsRelatedArtistsRequest.execute();

            List<Artist> newArtists = new ArrayList<>();
            for(Artist relatedArtist : relatedArtists){
                if (!usersSavedArtists.containsKey(relatedArtist.getName())) {
                    newArtists.add(relatedArtist);
                }
            }

            List<Artist> artistsSortedByPopularity = sortFoundArtistsByPopularity(newArtists);

            newArtistsMap.put(artistSimplified, artistsSortedByPopularity);
        }

        return newArtistsMap;
    }

    private List<Artist> sortFoundArtistsByPopularity(List<Artist> newArtists){
        return newArtists
                .stream()
                .sorted(Comparator.comparing(Artist::getPopularity).reversed()).collect(Collectors.toList());
    }
}
